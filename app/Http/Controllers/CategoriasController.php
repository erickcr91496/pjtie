<?php

namespace App\Http\Controllers;

use App\Categorias;
use Illuminate\Http\Request;
use Session;

class CategoriasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categorias  = Categorias::orderBy('id','Asc')->paginate(4);
       return view('categorias.index',compact('categorias'));
    // return view('categorias.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

  public function __construct(){

     $this->middleware('auth');

    }


    public function create()
    {
        return view('categorias.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'genero' => 'required',
            'talla' => 'required',
            'color' => 'required',
            ]);
            Categorias::create($request->all());
            
            Session::flash('message','Categoria Creada Correctamente');
            return redirect()->route('categorias.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Categorias  $categorias
     * @return \Illuminate\Http\Response
     */
    public function show(Categorias $categorias)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Categorias  $categorias
     * @return \Illuminate\Http\Response
     */
    public function edit(Categorias $categoria)
    {
        return view('categorias.edit',compact('categoria'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Categorias  $categorias
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Categorias $categoria)
    {
        $request->validate([
            'genero' => 'required',
            'talla' => 'required',
            'color' => 'required',
            ]);
         
         $categoria->update($request->all());
    
            Session::flash('message','Categoria  Actualizado Correctamente');
            return redirect()->route('categorias.index');
    }

    /** 
     * Remove the specified resource from storage.
     *
     * @param  \App\Categorias  $categorias
     * @return \Illuminate\Http\Response
     */
    public function destroy(Categorias $categoria)
    {
        $categoria->delete();
        Session::flash('message','Categoria Borrado Correctamente');
            return redirect()->route('categorias.index');
    }
}
