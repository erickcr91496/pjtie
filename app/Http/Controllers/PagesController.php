<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App;

class PagesController extends Controller
{

 public function __construct()
    {
        $this->middleware('auth');
    }


    public function inicio(){
        $notas = App\Nota::all();
        
         return view('welcome',compact('notas'));
        //return view('welcome');

    }

    // public function login(){
    //     return view('login');

    // }

     public function fotos(){
            return view('fotos');

     }
    public function nosotros($nombre = null){
        $equipo = ['Ignacio','Juanito','Pedrito'];

    return view('nosotros',compact('equipo','nombre'));
    }
    public function noticias(){
        return view('blog');

    }

}
