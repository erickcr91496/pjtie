<?php


namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App;


class ProveedorController extends Controller
{


    public function __construct(){
            $this->middleware('auth');


    }
    public function inicio(){
        $proveedores = App\Proveedor::all();
        
         return view('welcome',compact('proveedores'));
        
    }

    public function crear(Request $request){

     //   return $request->all();

        $request->validate([
            'id'=>'required',
            'nombre' => 'required',
            'direccion' =>  'required',
            'telefono' =>  'required'

        ]);

     $proveedor = new App\Proveedor;

     $proveedor->id = $request->id;
     $proveedor->NombreProv = $request->nombre;
     $proveedor->Direccion = $request->direccion;
     $proveedor->Telefono= $request->telefono;

     $proveedor->save();
     return back()->with('mensaje','Proveedor agregado !');
    }

    
    public function editar ($id){

            $proveedor = App\Proveedor::findOrFail($id);
            return view('proveedor.editar',compact('proveedor'));

    }

  public function update (Request $request,$id){

            $proveedorUpdate = App\Proveedor::findOrFail($id);
            $proveedorUpdate->id = $request->id;
            $proveedorUpdate->NombreProv = $request->nombre;
            $proveedorUpdate->Direccion = $request->direccion;
            $proveedorUpdate->Telefono = $request->telefono;
            $proveedorUpdate->Save();

            return back()->with('mensaje','Datos Actualizados');

    }

public function eliminar($id){

            $proveedorEliminar = App\Proveedor::findOrFail($id);
            $proveedorEliminar->delete();


            return back()->with('mensaje','Dato Eliminado');

}









}
