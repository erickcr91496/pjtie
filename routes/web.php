<?php

Auth::routes();

Route::get('/admin', 'ProveedorController@inicio' ) ;
// Route::get('/', 'PagesController@' ) ;

Route::post('/admin','ProveedorController@crear') ->name('proveedor.create');
Route::get('/editar/{id}','ProveedorController@editar')->name('proveedor.editar');

Route::put('/editar/{id}','ProveedorController@update')->name('proveedor.update');
Route::delete('eliminar/{id}','ProveedorController@eliminar')->name('proveedor.eliminar');

Route::resource('categorias', 'CategoriasController');


Route::get('fotos','PagesController@fotos')->name('foto');

Route::get('nosotros/{nombre?}','PagesController@nosotros')->name('nosotros');


//  Route::get('/', 'HomeController@index')->name('home');


// Route::get('password/reset','Auth\ForgotPassworController@showLinkRequestForm')
// ->name('password.request');

// Route::post('password/email','Auth\ForgotPassworController@sendResetLinkEmail')
// ->name('password.email');

// Route::get('password/reset/{token}','Auth\ResetPasswordController@showResetForm')
// ->name('password.reset');

// Route::post('password/reset','Auth\ResetPassworController@reset');


// Auth::routes();

