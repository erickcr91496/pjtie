@extends('categorias.layout')
@section('content')
<h1 class="text-center">Categorias </h1>
<div class="container">
    <a class="btn btn-info mb-3" href="{{route('categorias.create')}}">Categorias </a>
<table class="table">
    <thead class="thead-dark">
      <tr>
        <th scope="col">id</th>
        <th scope="col">Genero</th>
        <th scope="col">Talla</th>
        <th scope="col">Color</th>
        <th scope="col">Actualizar</th>
        <th scope="col">Eliminar</th>
      </tr>
    </thead>
    <tbody>
        @foreach ($categorias as $cat)
            
       
      <tr>
        <th scope="row">{{$cat->id}}</th>
        <td>{{$cat->genero}}</td>
        <td>{{$cat->talla}}</td>
        <td>{{$cat->color}}</td>
        <td><a class="btn btn-info" href="{{route('categorias.edit',$cat->id)}}"><i class="far fa-edit"></i></a></td>
      <td>
        <form action="{{route('categorias.destroy',$cat->id)}}" method="POST">
          @csrf
        @method('DELETE')
        <button type="submit" class="btn-sm btn-danger mt-3"><i class="far fa-trash-alt"></i> </button>
      </form>
        </td>
      </tr>
      @endforeach
     
    </tbody>
  </table>
  {{ $categorias->links() }}
</div>

@endsection