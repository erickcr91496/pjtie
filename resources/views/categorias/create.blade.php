@extends('categorias.layout')
@section('content')
<h1 class="text-center">Agregar Categorias </h1>
<hr>
<div class="container">


<form action="{{ route('categorias.store')}}" method="POST">
        @csrf

        <div class="row">
        
           <div class="col-md-12">
                <div class="form-group">
                    <strong> Genero:</strong>
            <input type="text" name="genero" class="form-control" placeholder=" Genero">
          </div>
          </div>
          <div class="col-md-12">
                <div class="form-group">
                        <strong> Talla</strong>
            <input type="text" name="talla"  class="form-control" placeholder="talla">
                </div>
           </div>
           <div class="col-md-12">
                <div class="form-group">
                        <strong> Color</strong>
            <input type="text" name="color" class="form-control"  placeholder="color">
             </div>
           </div>
          <div class="col-md-12 text-center" >
               <button type="submit" class="btn btn-primary">Enviar</button>
        
        </div>
        </div>    
      </form> 
    </div>

@endsection