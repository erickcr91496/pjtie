@extends('categorias.layout')
@section('content')
<h1 class="text-center">Agregar Categorias </h1>
<hr>
<div class="container">


<form action="{{route('categorias.update', $categoria->id)}}" method="POST">
        @csrf
        @method('PUT')

        <div class="row">
        
           <div class="col-md-12">
                <div class="form-group">
                    <strong> Genero:</strong>
            <input type="text" name="genero" value="{{$categoria->genero}}" class="form-control" placeholder=" Genero">
          </div>
          </div>
          <div class="col-md-12">
                <div class="form-group">
                        <strong> Talla</strong>
            <input type="text" name="talla" value="{{$categoria->talla}}" class="form-control" placeholder="talla">
                </div>
           </div>
           <div class="col-md-12">
                <div class="form-group">
                        <strong> Color</strong>
            <input type="text" name="color" value="{{$categoria->color}}" class="form-control"  placeholder="color">
             </div>
           </div>
          <div class="col-md-12 text-center" >
               <button type="submit" class="btn btn-primary">Enviar</button>
        
        </div>
        </div>    
      </form> 
    </div> 

@endsection