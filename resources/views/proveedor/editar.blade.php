@extends('plantilla')
@section('seccion')

 <h1>Editar</h1>
  @if (session('mensaje'))
      <div class="alert alert-success">
          {{ session('mensaje') }}
      </div>
  @endif
  <form action="{{ route('proveedor.update', $proveedor->id) }}" method="POST">
    @method('PUT')
    @csrf

    @error('id')
        <div class="alert alert-danger">
            El id es obligatorio
        </div>
    @enderror

    @error('nombre')
        <div class="alert alert-danger">
            El nombre es obligatoria
        </div>
    @enderror
    <input type="number" name="id" placeholder="Id proveedor" class="form-control mb-2" 
    value="{{ $proveedor->id }}">

    <input type="text" name="nombre" placeholder="Nombre" class="form-control mb-2"
     value="{{ $proveedor->NombreProv }}">
    <input type="text" name="direccion" placeholder="Direccion" class="form-control mb-2" 
    value="{{ $proveedor->Direccion }}">
    <input type="text" name="telefono" placeholder="Direccion" class="form-control mb-2" 
    value="{{ $proveedor->Telefono }}">
    <button class="btn btn-warning btn-block" type="submit">Editar</button>
  </form>
@endsection