
<!doctype html>
<html lang="en">
  <head>
            <!-- Required meta tags -->
    <meta charset="utf-8">
   <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

            <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

            <title>DIERSE-IE</title>
   </head>
     <body>
         <nav class="navbar navbar-expand-lg navbar navbar-dark bg-dark">
                        <a class="navbar-brand" href="#">DIERSE</a>
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                          <span class="navbar-toggler-icon"></span>
                        </button>

               <div class="collapse navbar-collapse mr-auto " id="navbarSupportedContent">
                    <ul class="navbar-nav  mr-auto">
                            <li class="nav-item active">
                              <a class="nav-link" href="">Inicio<span class="sr-only">(current)</span></a>
                            </li>
                            <li class="nav-item ">
                              <a class="nav-link" aria-disabled="false" href="">Catalogo</a>
                            </li>
                
                           
                            <li class="nav-item">
                              <a class="nav-link " href="#" tabindex="-1" >Productos</a>
                            </li>
                            <li class="nav-item">
                              <a class="nav-link " href="#" tabindex="-1" 
                              aria-disabled="false">Proveedores</a>
                            </li>
                              <li class="nav-item">
                              <a class="nav-link " href="#" tabindex="-1" >Categoria</a>
                            </li>
                            <li class="nav-item">
                              <a class="nav-link " href="#" tabindex="-1" >Inventario</a>
                            </li>
                                  <li class="nav-item">
                              <a class="nav-link " href="#" tabindex="-1" >Suscriptores</a>
                            </li>
                    </ul>
                                {{-- <ul class="nav justify-content-end">
                                          <div>
                                              <li class="nav-item dropdown ">
                                                <a class="nav-link dropdown-toggle  " href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" >
                                                Admin   </a>
                                              <div class="dropdown-menu " aria-labelledby="navbarDropdown">
                                                <a class="dropdown-item" href="#"></a>
                                                <a class="dropdown-item" href=""></a>
                                                <a class=" dropdown-item" href="#"></a>
                                              <a class="dropdown-item" href="{{route('logout')}}">Cerrar Sesion</a>
                                              </div>
                                                </li>
                                          </div>
                                         
                                                                           
                                  </ul>
                                   --}}
                                               <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                            <li class="nav-item">
                                {{-- <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a> --}}
                                <a class="nav-link" href="{{ route('login') }}">Inicie Sesion</a>

                            </li>
                            @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                </li>
                            @endif
                        @else
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Cierre sesion') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
        </nav>

        <div class="container my-4">
                  <h1 class="display-7">
                      Ingreso de Proveedores
                  </h1>

                  @if (session('mensaje'))
                  <div class="alert alert-success">
                                    {{ session('mensaje') }}
                    </div>
                  @endif
                            <form action="{{ route('proveedor.create')}}" method="post">
                              @csrf
                               @error('id')
                                  <div class="alert alert-danger">
                                    El id es obligatorio
                                  </div>
                              @enderror

                              @error('nombre')
                                  <div class="alert alert-danger">
                                    El nombre es obligatorio
                                  </div>
                              @enderror
                              @error('direccion')
                                  <div class="alert alert-danger">
                                   La descripcion es obligatorio
                                   <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                          <span aria-hidden="true">&times;</span>
                                  </button>
                                  </div>
                              @enderror
                              <input type="number" name="id" placeholder="Id Proveedor" class="form-control mb-2"
                              value={{ old('id')}}>
                              <input type="text" name="nombre" placeholder="Nombre" class="form-control mb-2"
                              value={{ old('nombre')}}>
                              <input type="text" name="direccion" placeholder="Descripcion" class="form-control mb-2"
                              value={{ old('direccion')}}>
                              <input type="text" name="telefono" placeholder="Telefono" class="form-control mb-2"
                              value={{ old('telefono')}}>

                              <button class="btn btn-success btn-block" type="submit">Agregar</button>
                            </form>
                    

                        <table class="table">
                      <thead>
                        <tr>
                          <th scope="col">id</th>
                          <th scope="col">Nombre</th>
                          <th scope="col">Descripcion</th>
                          <th scope="col">Telefono</th>
                          <th scope="col">Actualizar</th>
                          <th scope="col">Eliminar</th>
                        </tr>
                      </thead>
                      @foreach ($proveedores as $item)
                          
                        <tr>
                          <th scope="row">{{$item->id }}</th>
                          <td>{{$item->NombreProv}}</td>
                          <td>{{$item->Direccion}}</td>
                          <td>{{$item->Telefono}}</td>
                        <td><a class="btn btn-primary btn-sm" href="{{ route('proveedor.editar', $item) }}">Actualizar</a></td>
                      
                        <form action="{{ route('proveedor.eliminar', $item)}}" method="post">
                           @method('DELETE')
                            @csrf
                        <td><button class="btn btn-danger btn-sm" type="submit">
                          Eliminar</button>
                         </form>
                        </td>
                         
                        </tr>
                      @endforeach

                      </tbody>
                    </table>

        </div>
            <!-- Optional JavaScript -->
            <!-- jQuery first, then Popper.js, then Bootstrap JS -->
            <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
            <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
      </body>
</html>